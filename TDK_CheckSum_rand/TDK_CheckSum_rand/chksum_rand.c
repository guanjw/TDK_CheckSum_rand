/* 
 * chksum.c - Checksum and create ROM image HEX file from the random input Intel HEX file
 *
 * Description:	
 * It reads the inputfile (Intel MCS-386 w/ only Data, segment & End Records).
 * The maximum ROM size is set at 256KB max.  It can be changed later if needed.
 * If CRC is enable it calculates 16 bit CRC for all bytes in address range 0-(size-5),
 *   Stores the CRC-HI at (size-4) and CRC-LO at (size-3),
 * If checksum is enable it calculates 16 bit checksum for all bytes in address range 0-(size-3),
 *   Stores the checksum-HI at (size-2) and ChkSum-LO at (size-1).
 * Generates new outputfile.
 *
 * There is a "CHK.BAT" file in debug folder that can be used for regression test.
 *
 * Usage: "ChkSum  Ksize  [Opt] <inputfile.hex  >outputfile.hex"
 *
 *         Opt: 'b'= Generate both CRC & checksum  
 *              'c'= Generate only checksum or no option specified (default)
 *
 * CVS Revision History
 * $Revision $
 * $Header $
 * $Author $
 * $Date $
 * $Log: chksum.c,v $
* Revision 1.1  2008/03/04 01:11:47  tvander
 * *** empty log message ***
 *
 * Revision 1.4  2006/03/23 02:54:01  tnguyenl
 * Checksum version1p5 22-Mar-2006 - Optional CRC
 *
 * Revision 1.4  2006/03/22 22:13:33  tnguyenl
 * Checksum version1p3 works with TSC Flash Programmer 11-Nov-2005
 *
 * Revision 1.3  2005/10/26 00:58:41  tnguyenl
 * Added HEX-386 support (record type 4) for larger flash size (256KB) 25-Oct-2005
 *
 * Revision 1.2  2005/02/10 19:50:08  tnguyenl
 * Fixed 64KB boundary for Joanne
 *
 *
 */
/***************************************************************************
 *  Copyright (C) 2005 Teridian SemiConductor, Corp. (TSC) all rights reserved.
 *  This program is fully protected by the United States copyright
 *  laws and is the property of Teridian SemiConductor Corporation.
 **************************************************************************/


#include <stdio.h>
#include <stdlib.h>

#define	MAX_ROM_KSIZE	256

unsigned char Rom[MAX_ROM_KSIZE*1024];
unsigned long RomSize;
unsigned char *RomPtr;
char * msg;
char testmsg[] = "see it\0";

void Error(int err, int ln, char *msg)
{
	//if(!ln) fprintf(stderr, "\nUsage: ChkSum Ksize [Opt] <infile.hex >outfile.hex\n");
	fprintf(stderr, "\nERROR %d at input line %d - %s \n", err, ln, msg);
	exit(1);
}

int main(int argc, char * argv[])
{
	unsigned int	Seed;
	unsigned int	ByteCnt, AddrH, AddrL, RecTyp, Byte, LnCSRd, LnCSCal;
	unsigned int	SegAddr, SegH, SegL, Addr=0, RomChkSum, HighestAddr=0;
	unsigned int	i, ln=0;
	unsigned char	a, b, CRCh, CRCl, Opt='c';

	//msg = &testmsg[0];
	fprintf(stderr, "ChkSum Version 1.5 - 22-Mar-2006\n");
	
	if (argc < 2)  	
	{
		Error(1,ln,"\nUsage: ChkSum Ksize [Opt] <infile.hex >outfile.hex\n\n");
	    fprintf(stderr, "       Opt: ""b""= Generate both CRC and Checksum\n");
	    fprintf(stderr, "            ""c""= Generate Checksum\n");
	}

	i = sscanf(argv[1],"%d",&RomSize);
	if (i != 1)  	
	{
		Error(1,ln,"\nUsage: ChkSum Ksize [Opt] <infile.hex >outfile.hex\n\n");
	    fprintf(stderr, "       Opt: ""b""= Generate both CRC and Checksum\n");
	    fprintf(stderr, "            ""c""= Generate Checksum\n");
	}
	if (RomSize > MAX_ROM_KSIZE)  Error(1,ln,"Max. Rom size is 256KB");
	RomSize *= 1024;
	if (argc > 2)		sscanf(argv[2],"%c",&Opt);

	/* Initially fill Rom with FFs */
	RomPtr = &Rom[0];
	SegH = SegL = 0;
	for (i=0; i<RomSize; i++) *RomPtr++ = 0xFF;

	//增加随机数，按照实际传入参数的长度-2字节，添加随机数，因为最后两字节是校验码
   Seed = 2018;
   srand(Seed);
   for (i = 0; i < RomSize - 2; i++)
       Rom[ i ] = rand();

	/* Read the input hex file */
	ln=1;
	do
	{
		i = scanf(":%2x%2x%2x%2x",&ByteCnt,&AddrH,&AddrL,&RecTyp);
		if ( i < 4) Error(1,ln,"invalid HEX record or cmd line syntax error");
	
		if ( !(RecTyp==0 || RecTyp==1 || RecTyp==2 || RecTyp==4))
			Error(2,ln,"only HEX record types:0,1,2 & 4");
		
		if (RecTyp==4)		/* record 4 (HEX386) */
		{
			i = scanf("%2x%2x%2x\n", &SegH,&SegL,&LnCSRd);
			if ( i < 3) Error(4,ln,"invalid HEX record or cmd line syntax error");

			Addr = ((SegH << 24) + (SegL << 16) + (AddrH << 8) + AddrL);
			LnCSCal = ByteCnt + AddrH + AddrL + RecTyp + SegH + SegL;
			LnCSCal += LnCSRd;
			if ((LnCSCal & 0xFF) != 0)   Error(4,ln,"bad HEX386 record checksum");
		}

		else if (RecTyp==2) /* record 2 (HEX86) */
		{
			i = scanf("%2x%2x%2x\n", &SegH,&SegL,&LnCSRd);
			if ( i < 3) Error(4,ln,"invalid HEX record or cmd line syntax error");

			Addr = ( (SegH << 12) + (SegL << 4) + (AddrL & 0x0F) );
			LnCSCal = ByteCnt + AddrH + AddrL + RecTyp + SegH + SegL;
			LnCSCal += LnCSRd;
			if ((LnCSCal & 0xFF) != 0)   Error(4,ln,"bad HEX86 record checksum");
		}

		else				/* record 0 (data) */
		{
			Addr = ((SegH << 24) + (SegL << 16) + (AddrH << 8) + AddrL);
			LnCSCal = ByteCnt + AddrH + AddrL + RecTyp;
		
			for (i=0; i<ByteCnt; i++)
			{
				if (scanf("%2x",&Byte) != 1) Error(3,ln,"can't read data in data record");
				if (Addr > RomSize)         Error(3,ln,"data out of Rom space");
				Rom[Addr++] = Byte;
				LnCSCal += Byte;
			}
			if (scanf("%2x\n",&LnCSRd) != 1) Error(3,ln,"can't read record checksum");
			LnCSCal += LnCSRd;
			if ((LnCSCal & 0xFF) != 0)       Error(3,ln,"bad data record checksum");
		}

		ln++;
	}
	while (RecTyp != 1);	/*End Record*/


#if 1
    /* CRC enable and CRC locations are blank, OK to use */
	if (0xFF == (Rom[RomSize-4]&Rom[RomSize-3]) && Opt=='b')
	{	/* Calculate and Store Rom CRC */
		for (CRCh=CRCl=0, Addr=0; Addr<(RomSize-4); Addr++)
		{
			b = a = Rom[Addr] ^ CRCl;
			a <<= 4; b = a = b ^ a;
			a >>= 4; CRCl = a = a ^ CRCh;
			a = b << 3; CRCl ^= a;
			a = b >> 5; CRCh = a ^ b;
		}
		Rom[Addr++] = CRCh;
		Rom[Addr] = CRCl;

	} 
#endif

    /* Checksum locations are blank, OK to use */
	if (0xFF == (Rom[RomSize-2] & Rom[RomSize-1]))
	{	/* Calculate and Store Rom Check Sum */
		for (RomChkSum=0, Addr=0; Addr<(RomSize-2); Addr++)
			RomChkSum += Rom[Addr];
		Rom[Addr++] = RomChkSum >> 8;
		Rom[Addr] = RomChkSum;
	} 
	else 
		// Send warning but keep the output file
		fprintf(stderr, "\nWarning! Keeping existing file CRC/Checksum\n");

	/* Output new hex file for whole Rom */
	Addr = 0;
	while (Addr < RomSize)
	{
		if (Addr)
		{
			/* generate a HEX-386 segment record */
			LnCSCal = ( ~(02 + ((Addr >> 16)&0xFF) + ((Addr >> 8)&0xFF) + 04) + 1) & 0xFF;
			printf(":02000004%4.4X%2.2X\n", Addr >> 16, LnCSCal);	
		}
		for(SegAddr=0; (SegAddr < 0x10000 && SegAddr < RomSize); (SegAddr = SegAddr + 0x20))
		{
			printf(":20%4.4X00",SegAddr);		/* full length record size (32Bytes) */
			LnCSCal = 0x20 + (Addr >> 8) + (Addr & 0xFF);
			for (i=0; i<0x20; i++)
			{
				printf("%2.2X",Rom[Addr]);
				LnCSCal += Rom[Addr++];
			}
			LnCSCal = (~LnCSCal + 1) & 0xFF;
			printf("%2.2X\n",LnCSCal);
		}
		
	}
	printf(":00000001FF\n");

	return(0);
}
